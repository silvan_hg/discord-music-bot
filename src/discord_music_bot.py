import re
from typing import Optional
import discord
from discord.ext import commands
import yt_dlp
import os
from pytube import Playlist, YouTube
import datetime

# set discord bot TOKEN as environment variable for docker
TOKEN = os.getenv('DISCORD_BOT_TOKEN')
if TOKEN is None:
    raise ValueError("No token provided. Please set the DISCORD_BOT_TOKEN environment variable.")


# setup bot permissions
intents = discord.Intents.default()
intents.message_content = True
bot = commands.Bot(command_prefix='.', intents=intents, help_command=None)

# setup variables
url_list = []
title_list = []
vid_length_list = []
vc: Optional[discord.VoiceClient] = None
txt_channel_name = "music"


@bot.command()
async def help(ctx):
    embed = discord.Embed(title="Music Bot Help", description="This is a music bot. Use the following commands to "
                                                              "create and manage your playlist:", color=0x6B3FFF)

    embed.add_field(name=".join", value="Joins the voice channel of the user who sends the command", inline=False)
    embed.add_field(name=".play [URL]", value="Plays a song from the provided YouTube URL", inline=False)
    embed.add_field(name=".skip", value="Skips the currently playing song", inline=False)
    embed.add_field(name=".pause", value="Pauses the currently playing song", inline=False)
    embed.add_field(name=".resume", value="Resumes the paused song", inline=False)
    embed.add_field(name=".clear [playlist/channel]", value="Clears the playlist or the entire channel's messages",
                    inline=False)
    embed.add_field(name=".leave", value="Leaves the voice channel", inline=False)
    embed.add_field(name=".playlist", value="Displays the current playlist", inline=False)

    await ctx.send(embed=embed)


@bot.command()
async def echo(ctx, *args):
    arguments = ' '.join(args)
    await send_message(ctx, arguments)


async def send_message(ctx, message):
    e = discord.Embed(color=0x6B3FFF)
    footer_text = f"Command executed by: {ctx.author.name}\nMessage: {ctx.message.content}"
    e.set_footer(text=footer_text)
    e.set_author(name=message, icon_url="https://pics.craiyon.com/2023-11-03/41ad0f7f9c904e2d9524de53d8ddd8e6.webp")

    try:
        await ctx.message.delete()
    except discord.errors.NotFound:
        pass
    await ctx.send(embed=e)


@bot.command()
async def close(ctx):
    await send_message(ctx, "bot closed")
    await bot.close()
    exit()


@bot.command()
async def leave(ctx):
    await ctx.voice_client.disconnect()
    await send_message(ctx, "bot disconnected from voice channel")


@bot.command()
async def join(ctx):
    global vc

    if ctx.author.voice is None:
        await send_message(ctx, "you're not in a voice channel")
        return
    else:
        voice_channel = ctx.author.voice.channel
        if ctx.voice_client is None:
            vc = await voice_channel.connect()
        else:
            ctx.voice_client.stop()
            await vc.move_to(voice_channel)
        if url_list:
            play_next(ctx)


@bot.command()
async def pause(ctx):
    try:
        ctx.voice_client.pause()
        await send_message(ctx, "audio paused")
    except AttributeError:
        await send_message(ctx, "bot is not in your voice channel")
        pass


@bot.command()
async def resume(ctx):
    try:
        ctx.voice_client.resume()
        await send_message(ctx, "audio resumed")
    except AttributeError:
        await send_message(ctx, "bot is not in your voice channel")
        pass


@bot.command()
async def playlist(ctx):
    await ctx.message.delete()
    if url_list:
        e = discord.Embed(colour=0x6B3FFF, title="Playlist")
        title_str = ""
        field_count = 1
        songs_per_page = 10

        for i in range(len(title_list)):
            entry = f"_{i + 1}. {title_list[i]} {vid_length_list[i]}_\n"
            title_str += entry

            if (i + 1) % songs_per_page == 0:
                e.add_field(name=f"Page {field_count}", value=title_str, inline=True)
                field_count += 1
                title_str = ""

        if title_str:
            e.add_field(name=f"Page {field_count}", value=title_str, inline=True)

        await ctx.send(embed=e)
    else:
        await send_message(ctx, "playlist is empty")


@bot.command()
async def clear(ctx, *, arg=None):
    if not arg:
        await send_message(ctx, "argument required after .clear")
        return

    arg = arg.strip()
    match arg:
        case "playlist":
            await clear_playlist(ctx)
        case "channel":
            await clear_channel(ctx)
        case _:
            await send_message(ctx, f"invalid argument \'{arg}\'")


async def clear_playlist(ctx):
    url_list.clear()
    title_list.clear()
    vid_length_list.clear()
    await send_message(ctx, "playlist cleared")


@bot.command()
async def set_name(ctx, *args):
    global txt_channel_name
    txt_channel_name = ' '.join(args).strip()
    await send_message(ctx, f"name set to {txt_channel_name}")


async def clear_channel(ctx):
    if ctx.channel.name == txt_channel_name:
        await ctx.channel.purge()
    else:
        message = (f"clearing is only enabled in \'{txt_channel_name}\'\n"
                   f"you can change the channel by using .set_name [CHANNEL_NAME]")
        await send_message(ctx, message)


@bot.command()
async def skip(ctx):
    try:
        if vc.is_playing():
            vc.stop()
            await send_message(ctx, f"song {title_list[0]} skipped")
            play_next(ctx)
        else:
            await send_message(ctx, "bot is not playing a song")
    except AttributeError:
        await send_message(ctx, "bot is not in your voice channel")


def collect_playlist(url):
    for u in Playlist(url):
        url_list.append(u)
        title_list.append(YouTube(u).title)
        try:
            vid_length_list.append(str(datetime.timedelta(seconds=YouTube(u).length)))
        except Exception:
            vid_length_list.append("unknown")
    return len(Playlist(url))


def add_to_playlist(url):
    url_list.append(url)
    title_list.append(YouTube(url).title)
    try:
        vid_length_list.append(str(datetime.timedelta(seconds=YouTube(url).length)))
    except Exception:
        vid_length_list.append("unknown")


@bot.command()
async def play(ctx, url):
    url = url.strip()

    try:
        try:
            i = collect_playlist(url)
            await send_message(ctx, f"added {i} songs to playlist")
        except Exception:
            add_to_playlist(url)
            await send_message(ctx, f"added {title_list[-1]} to playlist")
        if vc:
            if ctx.voice_client.is_connected():
                if not vc.is_playing():
                    await send_message(ctx, f"now playing {title_list[-1]}")
                    play_next(ctx)

    except AttributeError:
        await send_message(ctx, f"added {title_list[-1]} to playlist")

    except Exception:
        await send_message(ctx, "could not load youtube url")


def play_next(ctx):
    try:
        if url_list:
            url = url_list.pop(0)
            title_list.pop(0)
            vid_length_list.pop(0)

            ydl_opts = {'format': 'bestaudio'}
            with yt_dlp.YoutubeDL(ydl_opts) as ydl:
                song_info = ydl.extract_info(url, download=False)

            ffmpeg_options = {'before_options': '-reconnect 1 -reconnect_streamed 1 -reconnect_delay_max 5',
                              'options': '-vn'}
            source = discord.FFmpegPCMAudio(song_info['url'], **ffmpeg_options)

            vc.play(source, after=lambda e: play_next(ctx))
    except Exception:
        # catch falsy url
        pass


bot.run(TOKEN)
